#ifndef DEBUGUTIL_H
#define DEBUGUTIL_H

#include <QDebug>

void inline printMatrix(float **matrix,int w,int h){
    for(int i=0;i<w;i++){
        QDebug db = qDebug();
        for(int j=0;j<h;j++){
            db<<matrix[i][j];
            db<<"\t";
        }
    }
    qDebug()<<"\n";
}

void inline printMatrix(long long int **matrix,int w,int h){
    for(int i=0;i<w;i++){
        QDebug db = qDebug();
        for(int j=0;j<h;j++){
            db<<matrix[i][j];
            db<<"\t";
        }
    }
    qDebug()<<"\n";
}

void inline printMatrix(double **matrix,int w,int h){
    for(int i=0;i<w;i++){
        QDebug db = qDebug();
        for(int j=0;j<h;j++){
            db<<matrix[i][j];
            db<<"\t";
        }
    }
    qDebug()<<"\n";
}

#endif // DEBUGUTIL_H
