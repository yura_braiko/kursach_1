#include "system.h"

#include <QStringList>
#include <QDebug>
#include <cmath>
#include "../util/debugutil.h"

System::System(QObject *parent, QString input) : QObject(parent)
{
    input.replace( " ", "" );
    this->source = input;
    matrix = nullptr;
    matrixHeigth = 0;
    matrixWidth = 0;
    updateMatrix();
}

System::MatrixInfo System::getMatrix()
{
    MatrixInfo result;
    result.matrix = this->matrix;
    result.heigth = matrixHeigth;
    result.width = matrixWidth;
    result.vars = vars;
    return result;
}

ValidationResult System::getError()
{
    return error;
}

QPair<QString, float> System::splitToNumAndLitera(QString in)
{
    QString num = "";
    QString litera = "";
    for(int i=0;i<in.length();i++){
        if(litera.length()<=0&&((in[i]>='0' && in[i] <='9')||(in[i]==','||in[i] == '.')) ){
            num+=in[i];
        }else{
            litera+=in[i];
        }
    }
    QPair<QString,float> result;
    result.first = litera;
    if(num!="")
        result.second = num.toFloat();
    else
        result.second = 1;
    return result;
}

void System::updateMatrix()
{
    if(source.length() == 0){
        for(int i=0;i<matrixWidth;i++)
            delete[] matrix[i];
        delete []matrix;
        matrixHeigth = matrixWidth = 0;
    }
    QStringList rows = source.split("\n");
    rowItem **parsedRows = new rowItem*[rows.length()];
    rowItem* currentRowItem;
    SetList *uniqueElement = new SetList();
    int k;
    for(int i=0;i<rows.length();i++){
        parsedRows[i] = new rowItem();
        currentRowItem =  parsedRows[i];
        QStringList sides = rows[i].split("=");
        if(sides.size() !=2 ||(sides.at(0).isEmpty() || sides.at(1).isEmpty())){
            qDebug()<<"ValidationResult::MissingRavno";
            error = ValidationResult::MissingRavno;
            return;
        }
        for(int e =0;e<2;e++){
            QStringList positive = sides.at(e).split("+");
            for(int j=0;j<positive.size();j++){
                if(positive.at(j).isEmpty())
                    continue;
                QStringList puts = positive.at(j).split("-");
                for(int p=0;p<puts.size();p++){
                    if(puts.at(p).isEmpty())
                        continue;
                    QPair<QString, float> split = splitToNumAndLitera(puts.at(p));
                    if(!split.first.isEmpty()){
                        uniqueElement->addNewItem(split.first);
                        k = (e ==0?1:-1);
                    }else {
                        uniqueElement->addNewItem(" ");
                        split.first = " ";
                        k = (e !=0?1:-1);
                    }
                    currentRowItem->num = (p==0?(k*split.second):-(k*split.second));
                    currentRowItem->var = split.first;
                    currentRowItem->next = new rowItem();
                    currentRowItem = currentRowItem->next;
                }
            }
        }
    }
    //-1 тому що выльний член рахуэться за змінну
    if(uniqueElement->getSize()-1>rows.size()){
        qDebug()<<uniqueElement->getSize() << rows.size();
        for(int i=0;i<uniqueElement->getSize();i++){
            qDebug()<<uniqueElement->toArray()[i];
        }
        error  =ValidationResult::ToMachVars;
        return;
    }
    matrixWidth = rows.size();
    matrixHeigth = uniqueElement->getSize();
    matrix  = new float*[matrixWidth];
    for(int i=0;i<matrixWidth;i++)
        matrix[i] = new float[matrixHeigth];
    QString *varsName = uniqueElement->toArray();
    for(int i=0;i<matrixHeigth;i++){
        this->vars.append(varsName[i]);
    }
    for(int i=0;i<matrixWidth;i++){
        for(int j=0;j<matrixHeigth;j++){
            matrix[i][j] = parsedRows[i]->getKoef(varsName[j]);
        }
    }
    printMatrix(matrix,matrixWidth,matrixHeigth);

    for(int i=0;i<rows.length();i++){
        delete parsedRows[i];
    }
    delete []parsedRows;
    delete uniqueElement;
}

void System::clearMatrix()
{
    for(int i=0;i<matrixWidth;i++){
        delete [] matrix[i];
    }
    delete []matrix;
    matrix = nullptr;
    matrixWidth =0;
}

System::SetList::SetList()
{
    rootElement = this;
}

System::SetList::~SetList()
{
    if(this == rootElement){
        SetList *iterator = nextElement;
        SetList *next = nextElement;
        while(iterator!=nullptr){
            next = iterator->nextElement;
            delete iterator;
            iterator = next;
        }
    }
}

void System::SetList::addNewItem(QString item)
{
    if(rootElement)
        rootElement->addItem(item);
    else
        addItem(item);
}

int System::SetList::getSize()
{
    if(this!=rootElement){
        return rootElement->getSize();
    }
    return rootElement->size;
}

QString *System::SetList::toArray()
{
    QString *result = new QString[getSize()];
    int i = 0;
    SetList *iterator = rootElement;
    while(iterator!=nullptr){
        result[i] = iterator->value;
        i++;
        iterator = iterator->nextElement;
    }
    return result;
}

System::SetList::SetList(System::SetList *parent, QString item)
{
    this->rootElement = parent;
    this->value = item;
    rootElement->size++;
}

void System::SetList::addItem(QString item)
{
    if(item == value) return;
    if(value.isEmpty()){
        this->value = item;
    }
    else{
        if(nextElement == nullptr)
            this->nextElement = new SetList(rootElement,item);
        else
            this->nextElement->addItem(item);
    }
}

System::rowItem::~rowItem()
{
    delete next;
}
