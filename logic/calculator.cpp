#include "calculator.h"
#include <cmath>
#include "../util/debugutil.h"

Calculator::Calculator(QObject *parent, System::MatrixInfo matrixInfo ) : QObject(parent)
{
    this->matrix = matrixInfo.matrix;
    this->width = matrixInfo.width;
    this->height = matrixInfo.heigth;
    this->vars = matrixInfo.vars;
    accuracy = 100;
}

QList<Calculator::Steps *> *Calculator::calculate()
{
    QList<Steps *>  *result = new QList<Steps *>();
    double **matrix = new double*[width];
    for(int i=0;i<width;i++){
        matrix[i] = new double[height];
        for (int j = 0; j < height; ++j) {
            matrix[i][j] = this->matrix[i][j];
        }
    }
    result->append(new Steps(matrix,-1,-1,width,height));
    long long nsk,k1,k2,nsd;
    int delta =1;

    for(int iteration= 0;iteration <2;iteration++){
        for(int i=1;i<width;i++){
            for(int j=0;j<height-1;j++){
                if(iteration ==0 && i <= j) continue;
                delta =1;
                qDebug()<<"i::j\t"<<i<<j <<"\t"<< delta;
                if((matrix[i][j]*matrix[i-delta][j]) ==0){
                    while (delta<=i && (matrix[i-delta][j] ==0)){
                        delta++;
                    }
                    if(delta>i || (matrix[i][j]*matrix[i-delta][j]) ==0){
                        continue;
                    }
                }
                qDebug()<<"i::j\t"<<i<<j <<"\t"<< delta;
                nsk = NSK(matrix[i][j],matrix[i-delta][j],accuracy);
                qDebug()<<"NSK" << matrix[i][j] <<matrix[i-delta][j]<< nsk;
                k1 = nsk/matrix[i][j];
                k2 = nsk/matrix[i-delta][j];
                for(int p=0;p<height;p++){
                    matrix[i][p] = matrix[i-delta][p]*k2 -matrix[i][p]*k1 ;
                }
                nsd = 0;
                for(int p=0;p<height;p++){
                    nsd = NSD(matrix[i][p],nsd ==0?matrix[i][p+1]:nsd,accuracy);
                }
                for(int p=0;p<height;p++){
                    matrix[i][p] /= nsd;
                }

                qDebug()<<"K :: " << k1 << k2;
                printMatrix(matrix,width,height);
                result->append(new Steps(matrix,i,i-delta,width,height));
                result->last()->k1 = k1;
                result->last()->k2 = k2;
            }
        }
        double *buf;
        for(int i=0;i<width/2;i++){
            buf = matrix[i];
            matrix[i] = matrix[width-i-1];
            matrix[width-i-1]=buf;
        }
    }
    saveLastResult(matrix);
    for(int i=0;i<width;i++)
        delete []matrix[i];
    delete []matrix;
    return result;
}

void Calculator::setAccuracy(int accuracy)
{
    this->accuracy = accuracy;
}

QList<QPair<QString, QString> > Calculator::getLastResult()
{
    return lastResult;
}


double Calculator::NSD(double a, double b, int accuracy)
{
    return ((double)(NSD(a*accuracy,b*accuracy)))/((double)accuracy);
}

double Calculator::NSK(double a, double b, int accuracy)
{
    return ((double)(NSK(a*accuracy,b*accuracy)))/((double)accuracy);
}

float Calculator::NSD(long long a, long long b)
{
    long long r;
    while (b != 0)
    {
        r = a % b;
        a = b;
        b = r;
    }
    return a;
}

float Calculator::NSK(long long a, long long b)
{
    if(a<0) a = -a;
    if(b<0) b = -b;
    return a / NSD(a, b) * b;

}

void Calculator::saveLastResult(double **matrix)
{
    lastResult.clear();
    double var=0;
    double result=0;
    for(int i=0;i<width;i++){
        for(int j=0;j<height-1;j++){
            if(matrix[i][j]!=0){
                var = matrix[i][j];
                result = matrix[i][height-1] / var;
                result = (int)(((result*((double)accuracy))));
                result = result/((double)accuracy);
                break;
            }
        }
        QPair<QString,QString> item;
        item.first = vars.at(i);
        item.second = QString::number(result);
        lastResult.append(item);
    }
}

