#ifndef CALCULATOR_H
#define CALCULATOR_H

#include "system.h"

#include <QObject>
#include <QList>
#include <QStringList>
#include <qpair.h>

class Calculator : public QObject
{
    Q_OBJECT

public:
    struct  Steps{
        Steps(double **matrix,int currentRow,int additionalRow,int w,int h){
            this->matrix = new float*[w];
            for(int i=0;i<w;i++){
                this->matrix[i] = new float[h];
                for (int j = 0; j < h; ++j) {
                    this->matrix[i][j] = matrix[i][j];
                }
            }
            this->currentRow = currentRow;
            this->additionalRow = additionalRow;
            this->w = w;
            this->h =h;
            this->k1 = 1;
            this->k2 = 1;
        }
        float **matrix;
        int currentRow;
        int additionalRow;
        int w,h;
        int k1,k2;


    };

public:
    explicit Calculator(QObject *parent = 0,System::MatrixInfo matrixInfo =  System::MatrixInfo() );

    QList<Steps*> *calculate();
    void setAccuracy(int accuracy);
    QList<QPair<QString,QString>> getLastResult();


signals:

public slots:
private:
    float **matrix  ;
    int width ;
    int height;
    int accuracy;
    QStringList vars;
    QList<QPair<QString,QString>> lastResult;

    double NSD(double a,double b,int accuracy);
    double NSK(double a,double b,int accuracy);

    float NSD(long long int a,long long int b);
    float NSK(long long int a,long long int b);


   void  saveLastResult(double **matrix);

};

#endif // CALCULATOR_H
