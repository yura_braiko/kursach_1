#ifndef SYSTEM_H
#define SYSTEM_H

#include <QObject>
#include <QPair>


enum class ValidationResult
{
    Normal=1,
    MissingRavno = 2,
    ToMachVars = 4
};

class System : public QObject
{
    Q_OBJECT
    struct rowItem{
        rowItem *next = nullptr;
        QString var= "";
        float num = 0;

        /**
         * @brief getKoef коефіцієнт для конкретної змінної
         * @param value ім'я змінної
         * @return занчення коєфіцієнту, або 0  якщо ні цей об'єкт ні наступні не мають такої змінної
         */
        inline float getKoef(QString value){
            float result = 0;
            rowItem *iterator = this;
            while(iterator != nullptr){
                if(iterator->var == value){
                    result+=iterator->num;
                }
                iterator = iterator->next;
            }
            return result;
        }
        ~rowItem();
    };

    class SetList{
    public:
        SetList();
        ~SetList();
        void addNewItem(QString item);
        int getSize();
        QString *toArray();

    private:
        SetList *rootElement = nullptr;
        SetList *nextElement = nullptr;
        QString value;
        int size = 1;

        SetList(SetList *parent,QString item);
        void addItem(QString item);
    };

public:
    struct MatrixInfo{
        MatrixInfo(){
            matrix = nullptr;
            width = 0;
            heigth = 0;

        }

        float **matrix;
        int width;
        int heigth;
        QStringList vars;
    };

public:
    explicit System(QObject *parent = 0,QString input="");
    //add check to error;
    MatrixInfo getMatrix();
    ValidationResult getError();

signals:

public slots:

private:
    QString source;
    int matrixWidth, matrixHeigth;
    float **matrix;
    ValidationResult error = ValidationResult::Normal;
    QPair<QString, float> splitToNumAndLitera(QString in);
    void updateMatrix();
    void clearMatrix();
    QStringList vars;

};

#endif // SYSTEM_H
