#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "logic/calculator.h"
#include "printer/printer.h"
#include "printer/printerwithimage.h"
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QPrinter>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    accuracy = 100;

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_loadFileBtn_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,  tr("Відкрити "), "~/", tr("Текстові файли (*.txt)"));
    QFile file(fileName);
    file.open(QFile::ReadOnly);
    QTextStream textStream(&file);
    QString text = textStream.readAll();
    file.close();
    calculate(text);
}

void MainWindow::calculate(QString str, bool isImageEnable)
{
    ui->sourc->setPlainText(str);
    ui->result->setText("");
    calculate(str,ui->result->document(),isImageEnable);

}

void MainWindow::calculate(QString str, QTextDocument *doc, bool isImageEnable)
{
    System sys(this,str);
    System::MatrixInfo info = sys.getMatrix();
    if(sys.getError()!=ValidationResult::Normal){
        showParseMatrixError(sys.getError());
        return;
    }
    Calculator calc(this,info);
    calc.setAccuracy(accuracy);
    Printer *printer;
    if(isImageEnable)
        printer = new PrinterWithImage(this, &calc);
    else
        printer =new  Printer(this, &calc);
    printer->insertCalculation(doc);
    printer->insertResult(doc);
    delete printer;
}

void MainWindow::on_calculate_clicked()
{
    calculate(ui->sourc->toPlainText());
}

void MainWindow::on_loadAsText_clicked()
{
    QString text = ui->sourc->toPlainText();
    QString fileName = QFileDialog::getSaveFileName(this,  tr("Зберегти "), "~/", tr("Текстові файли (*.txt)"));
    QFile file(fileName);
    file.open(QFile::WriteOnly);
    QTextStream textStream(&file);
    QTextDocument doc;
    calculate(text,&doc,false);
    textStream<<doc.toPlainText();
    file.close();
}

void MainWindow::on_loadAsPdf_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,  tr("Зберегти "), "~/", tr("Текстові файли (*.pdf)"));
    QPrinter printerPdf(QPrinter::PrinterResolution);
    printerPdf.setOutputFormat(QPrinter::PdfFormat);
    printerPdf.setPaperSize(QPrinter::A4);
    printerPdf.setOutputFileName(fileName);
    printerPdf.setPageMargins(QMarginsF(30, 30, 30, 30));
    QString text = ui->sourc->toPlainText();
    QTextDocument doc;
    calculate(text,&doc);
    doc.print(&printerPdf);
}

void MainWindow::showParseMatrixError(ValidationResult error)
{
    QString str;

    qDebug()<<"ValidationResult::MissingRavno";
    switch (error) {
    case ValidationResult::MissingRavno:
        str = "В наведеному тексті неможливо розпарсити систему лінійних алгібраїчних рівнянь";
        break;
    case ValidationResult::ToMachVars:
        str = "Кілкість змінних повинна відповідати кількості рівнянь";
        break;
    default:
        str = "невідома помилка";
        break;
    }
    QMessageBox msgBox;
    msgBox.setText("Помилка");
    msgBox.setInformativeText(str);
    msgBox.exec();
}

void MainWindow::on_accuracy_textChanged(const QString &arg1)
{
    QString str = arg1;
    for(int i=0;i<arg1.length();i++){
        if(arg1[i]<'0'||arg1[i]>'9'){
            str.remove(i,1);
            ui->accuracy->setText(str);
            return;
        }
    }
    accuracy = arg1.toInt();
}
