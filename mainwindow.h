#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextDocument>
#include "logic/system.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_loadFileBtn_clicked();
    void calculate(QString str,bool isImageEnable = true);
    void calculate(QString str,QTextDocument *doc,bool isImageEnable = true);

    void on_calculate_clicked();

    void on_loadAsText_clicked();

    void on_loadAsPdf_clicked();

    void on_accuracy_textChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;
    void showParseMatrixError(ValidationResult error);
    int accuracy;
};

#endif // MAINWINDOW_H
