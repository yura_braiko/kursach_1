#include "printer.h"

#include <QtGui/qtextcursor.h>
#include <QDebug>

Printer::Printer(QObject *parent, Calculator *calculator) : QObject(parent)
{
    this->calculator = calculator;
    if(calculator!=nullptr)
        this->steps = calculator->calculate();
    else
        this->steps = new QList<Calculator::Steps *>();
    currentFont = QFont("times", 14);
}

QTextDocument * Printer::insertCalculation(QTextDocument *doc)
{
    QTextCursor* cursor = new QTextCursor(doc);
    cursor->insertText("    Перепишемо систему лінійних алгібраїчних рівнянь в матричну форму: ");
    cursor->insertBlock();
    insertMatrix(steps->first(),doc,cursor);
    cursor->insertBlock();
    for(int i=1;i<steps->size();i++){
        Calculator::Steps *currentStep = steps->at(i);
        cursor->insertText("    Домножимо строку ");
        cursor->insertText(QString::number(currentStep->additionalRow+1));
        cursor->insertText(" на ");
        cursor->insertText(QString::number(currentStep->k2));
        cursor->insertText(",а строку ");
        cursor->insertText(QString::number(currentStep->currentRow+1));
        cursor->insertText(" на ");
        cursor->insertText(QString::number(currentStep->k1));
        cursor->insertText(".");
        cursor->insertText(" Від ");
        cursor->insertText(QString::number(currentStep->additionalRow+1));
        cursor->insertText(" строки віднімемо ");
        cursor->insertText(QString::number(currentStep->currentRow+1));
        cursor->insertText(" строку");
        cursor->insertText(" і результат запишемо в ");
        cursor->insertText(QString::number(currentStep->currentRow+1));
        cursor->insertText(" строку.");
        cursor->insertBlock();
        cursor->insertText("    Отримаємо нову матрицю: ");
        cursor->insertBlock();
        insertMatrix(steps->at(i),doc,cursor);
        cursor->insertBlock();
    }
    return doc;
}

QTextDocument *Printer::insertResult(QTextDocument *doc)
{

    QTextCursor* cursor = new QTextCursor(doc);
    cursor->movePosition(QTextCursor::End);
    QList<QPair<QString, QString> > results = calculator->getLastResult();
    qDebug()<<results.size();
    cursor->insertBlock();
    cursor->insertText("Відповідь : ");
    cursor->insertBlock();
    for(int i=0;i<results.size();i++){
        cursor->insertText("\t");
        cursor->insertText(results.at(i).first);
        cursor->insertText(" = ");
        cursor->insertText(results.at(i).second);
        cursor->insertBlock();
    }
    qDebug()<<doc->toPlainText();
    return doc;
}

void Printer::insertMatrix(Calculator::Steps *step,QTextDocument *, QTextCursor *cursor)
{
    for(int i=0;i<step->w;i++){
        cursor->insertText("\t");
        for (int j = 0; j < step->h; ++j) {
            cursor->insertText(QString::number(step->matrix[i][j]));
            if(j ==step->h-2)
                cursor->insertText("|");
            cursor->insertText(" ");
        }
        cursor->insertBlock();
    }
}
