#ifndef PRINTERWITHIMAGE_H
#define PRINTERWITHIMAGE_H

#include "printer.h"

class PrinterWithImage:public Printer
{
public:
    PrinterWithImage(QObject *parent = 0,Calculator *Calculator = nullptr);

    // Printer interface
protected:
    void insertMatrix(Calculator::Steps *step, QTextDocument *doc, QTextCursor *cursor);
};

#endif // PRINTERWITHIMAGE_H
