#include "printerwithimage.h"

#include <QFont>
#include <QPrinter>
#include <QImage>
#include <QDebug>
#include <QtGui/qtextcursor.h>
#include <QFontMetrics>
#include <QImageReader>
#include <QImageWriter>
#include <QPainter>
#include <QTextDocument>

PrinterWithImage::PrinterWithImage(QObject *parent, Calculator *Calculator):Printer(parent,Calculator)
{
}

void PrinterWithImage::insertMatrix(Calculator::Steps *step, QTextDocument *, QTextCursor *cursor)
{
    QString ** text = new QString*[step->w];
    int longestText = 0;
    currentFont.setPointSize(16);
    QFontMetrics fm(currentFont);
    int parenthesisSize = 25;

    for(int i=0;i<step->w;i++){
        text[i] = new QString[step->h];
        for(int j=0;j<step->h;j++){
            text[i][j] = QString::number(step->matrix[i][j]);
            while(text[i][j].length()<3 && j!=step->h-1)
                text[i][j]+=" ";
            qDebug()<<text[i][j];
            if(fm.width(text[i][j])>longestText)
                longestText = fm.width(text[i][j]);
        }
        qDebug()<<"_";
    }
    int imageW = longestText*step->h+fm.width(" ")*(step->h)+parenthesisSize*2;
    int imageH = fm.height()*(step->w);
    QImage result =QImage(imageW, imageH, QImage::Format_ARGB32_Premultiplied);
    result.fill(QColor::fromRgba(qRgba(255,255,255,255)));
    QPainter paint(&result);
    paint.setRenderHints(QPainter::HighQualityAntialiasing );
    paint.setFont(currentFont);

    int offsetX =parenthesisSize+fm.width(" ")/2;
    int offsetY =fm.ascent();
    int spaceLenth = fm.width(" ");
    for(int i=0;i<step->w;i++){
        for(int j=0;j<step->h;j++){
            qDebug()<<offsetX<<offsetY<<text[i][j];
            paint.drawText(offsetX,offsetY,text[i][j]);
            offsetX+=longestText+spaceLenth;
        }
        offsetY+=fm.height();
        offsetX = parenthesisSize+fm.width(" ")/2;
    }
    paint.drawEllipse(QPointF(((float)result.width())/2,((float)result.height())/2),result.width()/2-parenthesisSize/2,result.width()/2-parenthesisSize/2);
    paint.drawLine(parenthesisSize + (longestText+spaceLenth)*(step->h-1),fm.height()/3,parenthesisSize+(longestText+spaceLenth)*(step->h-1),result.height()-fm.height()/3);
    cursor->insertBlock();
    QTextBlockFormat format;
    format.setAlignment(Qt::AlignHCenter);
    cursor->setBlockFormat(format);
    cursor->insertImage(result);
    cursor->insertBlock();
    format.setAlignment(Qt::AlignLeft);
    cursor->setBlockFormat(format);

}
