#ifndef PRINTER_H
#define PRINTER_H

#include <QObject>
#include "../logic/calculator.h"
#include <QTextDocument>
#include <QFont>

class Printer : public QObject
{
    Q_OBJECT
public:
    explicit Printer(QObject *parent = 0,Calculator *calculator = nullptr);
    QTextDocument * insertCalculation(QTextDocument *cursor);
    QTextDocument *insertResult(QTextDocument *cursor);

signals:

public slots:

protected:
    QList<Calculator::Steps *> *steps;
    QFont currentFont;
    Calculator *calculator;

    virtual void insertMatrix(Calculator::Steps * step, QTextDocument *doc, QTextCursor* cursor);

};

#endif // PRINTER_H
