#-------------------------------------------------
#
# Project created by QtCreator 2016-05-23T22:22:17
#
#-------------------------------------------------

QT       += core printsupport gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = kursach
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    logic/system.cpp \
    logic/calculator.cpp \
    printer/printer.cpp \
    printer/printerwithimage.cpp

HEADERS  += mainwindow.h \
    logic/system.h \
    util/debugutil.h \
    logic/calculator.h \
    printer/printer.h \
    printer/printerwithimage.h

FORMS    += mainwindow.ui
